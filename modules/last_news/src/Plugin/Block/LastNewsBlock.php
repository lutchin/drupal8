<?php
/**
 * Created by PhpStorm.
 * User: Pluchenok
 * Date: 09.08.2017
 * Time: 14:31
 */



/**
 * @file
 * Contains \Drupal\last_news\Plugin\Block\LastNewsBlock.
 */


namespace Drupal\last_news\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * @Block(
 *   id = "last_news_block",
 *   admin_label = @Translation("Last News Block"),
 * )
 */
class LastNewsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $block = [
      '#type' => 'markup',
      '#markup' => '<div id="last-comments">' . _last_news_block_content() . '</div>'
    ];
    return $block;
  }

  function _last_news_block_content($from_cid = NULL) {
    // Выбираем комментарии
    $query = \Drupal::database()->select('node__body', 'nb')
      ->fields('nb', array('entity_id', 'body_value'))
      ->orderBy('entity_id', 'DESC')
      ->range(0, 200);

    if (!is_null($from_cid)) {
      $query->condition('entity_id', $from_cid, '<');
    }

    $items = array();
    foreach ($query->execute() as $news) {
      $items[] = l($news->body_value, 'node/' . $news->entity_id, array('fragment' => 'news-' . $news->entuty_id));
      $last_cid = $news->entity_id;
    }

    $content = theme('item_list', array('items' => $items));
    $content .= '<div class="more-link">' . l('more', 'last-news/' . $last_cid . '/nojs', array('attributes' => array('class' => array('use-ajax'), 'title' => 'More comments'))) . '</div>';

    return $content;
  }

}